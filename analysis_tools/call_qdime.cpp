
#include "pin.H"
#include "portability.H"
#include <vector>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdio.h>
#include <unistd.h> 
#include <time.h>
#include <unordered_set>
#include <set>
#include "qdime.h"

string File_Name = "call_qdime";//output file name
/* ===================================================================== */
/* Commandline Switches */
/* ===================================================================== */

KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE,         "pintool",
    "o", "call.out", "trace file");
KNOB<BOOL>   KnobPid(KNOB_MODE_WRITEONCE,                "pintool",
    "i", "0", "append pid to output");
KNOB<THREADID>   KnobWatchThread(KNOB_MODE_WRITEONCE,                "pintool",
    "watch_thread", "-1", "thread to watch, -1 for all");
KNOB<BOOL>   KnobFlush(KNOB_MODE_WRITEONCE,                "pintool",
    "flush", "0", "Flush output after every instruction");
KNOB<BOOL>   KnobSymbols(KNOB_MODE_WRITEONCE,       "pintool",
    "symbols", "1", "Include symbol information");
KNOB<BOOL>   KnobLines(KNOB_MODE_WRITEONCE,       "pintool",
    "lines", "0", "Include line number information");
KNOB<BOOL>   KnobTraceInstructions(KNOB_MODE_WRITEONCE,       "pintool",
    "instruction", "0", "Trace instructions");
KNOB<BOOL>   KnobTraceCalls(KNOB_MODE_WRITEONCE,       "pintool",
    "call", "1", "Trace calls");
KNOB<BOOL>   KnobTraceMemory(KNOB_MODE_WRITEONCE,       "pintool",
    "memory", "0", "Trace memory");
KNOB<BOOL>   KnobSilent(KNOB_MODE_WRITEONCE,       "pintool",
    "silent", "0", "Do everything but write file (for debugging).");
KNOB<BOOL> KnobEarlyOut(KNOB_MODE_WRITEONCE, "pintool", "early_out", "0" , "Exit after tracing the first region.");

/* ===================================================================== */
/* Global Variables */
/* ===================================================================== */
typedef UINT64 COUNTER;
LOCALVAR std::ofstream out;
LOCALVAR INT32 enabled = 1;
LOCALFUN BOOL Emit(THREADID threadid)
{
    if (!enabled || 
        KnobSilent || 
        (KnobWatchThread != static_cast<THREADID>(-1) && KnobWatchThread != threadid))
        return false;
    return true;
}
//LOCALFUN VOID Flush()
//{
//    if (KnobFlush) {
//		//out << flush;
//		fflush(Trace_File);
//	}
//}
LOCALFUN VOID Fini(int, VOID * v);

/* ===================================================================== */
#define sec_to_nsec 1000000000//from second to nanosecond
struct timespec tm;//print timestamp to measure info gain with time
//save unique traces in unordered set; it's faster than set
std::unordered_set<string> Trace_Set;
std::unordered_set<string>::const_iterator Iter_Trace;
//save timestamps in ordered set; I care about the ascending order
std::set<long double> Time_Set;
std::set<long double>::const_iterator Iter_Time;
//Check if the extracted trace is unique (not redundant)
static inline void CheckIfUnique(string str)
{
	clock_gettime( CLOCK_MONOTONIC, &tm);
	if ( Trace_Set.find(str) == Trace_Set.end())
	{	
		//if not found i.e., new unique trace
		//add the trace to the unordered_set 
		Trace_Set.insert(str);
		//and add the timestamp to the set
		Time_Set.insert(tm.tv_sec + (long double)tm.tv_nsec/sec_to_nsec);
	}
}

/* ===================================================================== */

VOID EmitDirectCall(THREADID threadid, string * str, INT32 tailCall/*, ADDRINT arg0, ADDRINT arg1*/)
{
    if (!Emit(threadid))
        return;
    ThreadData* tdata = get_tls(threadid);
    fprintf(tdata->Trace_File, "%s\n", (*str).c_str() );    
    PIN_LockClient();
    CheckIfUnique(*str);//ignore thread id
    PIN_UnlockClient();
}

string FormatAddress(ADDRINT address, RTN rtn)
{
    string s = "";// StringFromAddrint(address);
    
    if (KnobSymbols && RTN_Valid(rtn))
    {
        s += " " /*+ IMG_Name(SEC_Img(RTN_Sec(rtn))) + ":"*/;
        s += RTN_Name(rtn);
    }
	else if (KnobSymbols && !RTN_Valid(rtn))
	{
		s += " ";
		s += "invalid";
	}
    return s;
}

VOID EmitIndirectCall(THREADID threadid, string * str, ADDRINT target/*, ADDRINT arg0, ADDRINT arg1*/)
{
    if (!Emit(threadid))
        return;
    PIN_LockClient();
    string s = FormatAddress(target, RTN_FindByAddress(target));
    CheckIfUnique((*str) + s);
    PIN_UnlockClient();
	ThreadData* tdata = get_tls(threadid);
	fprintf(tdata->Trace_File, "%s%s\n", (*str).c_str(), s.c_str() );

}

VOID EmitReturn(THREADID threadid, string * str/*, ADDRINT ret0*/)
{
    if (!Emit(threadid))
        return;
    ThreadData* tdata = get_tls(threadid);
    fprintf(tdata->Trace_File, "%s\n", (*str).c_str() );
	PIN_LockClient();
    CheckIfUnique(*str);
    PIN_UnlockClient();
}

        
VOID CallTrace(TRACE trace, INS ins)
{
    if (!KnobTraceCalls)
        return;

    if (INS_IsCall(ins) && !INS_IsDirectBranchOrCall(ins))
    {
        // Indirect call
        string s = "C" + FormatAddress(INS_Address(ins), TRACE_Rtn(trace));
        s += " ";
        INS_InsertPredicatedCall(ins, IPOINT_BEFORE, AFUNPTR(EmitIndirectCall), IARG_THREAD_ID,
                       IARG_PTR, new string(s), IARG_BRANCH_TARGET_ADDR, IARG_END);
    }
    else if (INS_IsDirectBranchOrCall(ins))
    {
        // Is this a tail call?
        RTN sourceRtn = TRACE_Rtn(trace);
        RTN destRtn = RTN_FindByAddress(INS_DirectBranchOrCallTargetAddress(ins));

        if (INS_IsCall(ins)         // conventional call
            || sourceRtn != destRtn // tail call
        )
        {
            BOOL tailcall = !INS_IsCall(ins);
            
            string s = "";
            if (tailcall)
            {
                s += "T";
            }
            else
            {
                if( INS_IsProcedureCall(ins) )
                    s += "C";
                else
                {
                    s += "PcMaterialization";
                    tailcall=1;
                }
                
            }
            //s += INS_Mnemonic(ins) + " ";
            s += FormatAddress(INS_Address(ins), TRACE_Rtn(trace));
            //s += " ";
            ADDRINT target = INS_DirectBranchOrCallTargetAddress(ins);
            s += FormatAddress(target, RTN_FindByAddress(target));
            INS_InsertPredicatedCall(ins, IPOINT_BEFORE, AFUNPTR(EmitDirectCall),
                           IARG_THREAD_ID, IARG_PTR, new string(s), IARG_BOOL, tailcall, IARG_END);
        }
    }
    else if (INS_IsRet(ins))
    {
        RTN rtn =  TRACE_Rtn(trace);
#if defined(TARGET_LINUX) && defined(TARGET_IA32)
//        if( RTN_Name(rtn) ==  "_dl_debug_state") return;
        if( RTN_Valid(rtn) && RTN_Name(rtn) ==  "_dl_runtime_resolve") return;
#endif
        string tracestring = "R" + FormatAddress(INS_Address(ins), rtn);
        INS_InsertPredicatedCall(ins, IPOINT_BEFORE, AFUNPTR(EmitReturn),
                       IARG_THREAD_ID, IARG_PTR, new string(tracestring), IARG_END);
    } 
}
     
/* ===================================================================== */

VOID Trace(TRACE trace, VOID *v)
{
	UINT64 trace_addr = TRACE_Address(trace);
	IMG img = IMG_FindByAddress(trace_addr);
	if(!IMG_Valid(img)) return;
	ADDRINT version = TRACE_Version(trace);
	UINT64 img_low_addr = IMG_LowAddress(img);
	UINT64 trace_rel_addr = trace_addr - img_low_addr;
	USIZE trace_size = TRACE_Size(trace);
	THREADID thread_id = PIN_ThreadId();
	bool Allow_Instrum = 1;
	if(Redun_Suppress)
	{
		Allow_Instrum = dime_compare_to_log(thread_id, trace_rel_addr, trace_size);
	}
	
	if(Allow_Instrum)
	{
		for (BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl))
		{
		    for (INS ins = BBL_InsHead(bbl); INS_Valid(ins); ins = INS_Next(ins))
		    {
		    	if(INS_IsCall(ins) || INS_IsDirectBranchOrCall(ins) || INS_IsRet(ins))
		    	{
		        	dime_switch_version(version, ins);
					switch(version) {
					  case VERSION_BASE:
					  	//Do Nothing 
						break;
					  case VERSION_INSTRUMENT:
						CallTrace(trace, ins);
						break;
					  default:
						assert(0);
						break;
					}
		        }
		    }
		}
		dime_modify_log(version, thread_id, trace_rel_addr, trace_size);
    }
}
/* ===================================================================== */
VOID ThreadStart(THREADID threadid, CONTEXT *ctxt, INT32 flags, VOID *v)
{
	dime_thread_start(threadid);
	ThreadData* tdata = get_tls(threadid);
	ostringstream os;
	string file_name = File_Name;
	os << (threadid+1);
	file_name += os.str();//thread id
	file_name += ".out";
	tdata->Trace_File = fopen(file_name.c_str(),"w");
}
/* ===================================================================== */

VOID Fini(int, VOID * v)
{
	dime_fini();
	//Close Trace_File of each thread
	ThreadData* tdata;
	for(int t = 0; t < Num_Threads; t++)
	{
	 	tdata = get_tls(t);
	 	fclose(tdata->Trace_File);
	}
	//print timestamps of unique traces
	FILE* tm_file = fopen("call_qdime_tm.txt", "w");
	for ( auto it = Time_Set.begin(); it != Time_Set.end(); ++it )
	{
		fprintf(tm_file, "%.9Lf\n", *it);
	}
	fclose(tm_file);
}

/* ===================================================================== */

int main(int argc, CHAR *argv[])
{
    PIN_Init(argc,argv);
	dime_init((char*)"call_qdime_extra.out");
    PIN_AddThreadStartFunction(ThreadStart, 0);
    TRACE_AddInstrumentFunction(Trace, 0);
    PIN_AddFiniFunction(Fini, 0);
    // Never returns
    PIN_StartProgram();
    return 0;
}

/* ===================================================================== */
/* eof */
/* ===================================================================== */
