/*
 *  This file contains an ISA-portable PIN tool for tracing system calls
 */

#include <stdio.h>

#if defined(TARGET_MAC)
#include <sys/syscall.h>
#elif !defined(TARGET_WINDOWS)
#include <syscall.h>
#endif

#include "pin.H"
#include <time.h>
#include <unordered_set>
#include <set>
#include <iostream>
#include <sstream>

string File_Name = "strace";//output file name

// Create a Trace_File for each thread to avoid racing
INT32 Num_Threads = 0;
static TLS_KEY Tls_Key;
PIN_LOCK Lock;
class ThreadData
{
  public:
    	FILE* Trace_File;
};
// function to access thread-specific data
ThreadData* get_tls(THREADID thread_id)
{
    ThreadData* tdata = 
          static_cast<ThreadData*>(PIN_GetThreadData(Tls_Key, thread_id));
    return tdata;
}
/* ===================================================================== */
#define sec_to_nsec 1000000000//from second to nanosecond
struct timespec tm;//print timestamp to measure info gain with time
//save unique traces in unordered set; it's faster than set
std::unordered_set<string> Trace_Set;
std::unordered_set<string>::const_iterator Iter_Trace;
//save timestamps in ordered set; I care about the ascending order
//timestamps of unique traces
std::set< double> Time_Unq_Set;
std::set< double>::const_iterator Iter_Unq_Time;
//Check if the extracted trace is unique (not redundant)
//long double test[10000000];
//int indx = 0;
static inline void CheckIfUnique(string str)
{
	clock_gettime( CLOCK_MONOTONIC, &tm);
	if ( Trace_Set.find(str) == Trace_Set.end())
	{	
		//if not found i.e., new unique trace
		//add the trace to the unordered_set 
		Trace_Set.insert(str);
		//and add the timestamp to the set
		Time_Unq_Set.insert(tm.tv_sec + ( double)tm.tv_nsec/sec_to_nsec);
	}
}
/* ===================================================================== */
// Print syscall number and arguments
VOID SysBefore(ADDRINT ip, ADDRINT num, ADDRINT arg0, ADDRINT arg1, ADDRINT arg2, ADDRINT arg3, ADDRINT arg4, ADDRINT arg5, THREADID threadid)
{
#if defined(TARGET_LINUX) && defined(TARGET_IA32) 
    // On ia32 Linux, there are only 5 registers for passing system call arguments, 
    // but mmap needs 6. For mmap on ia32, the first argument to the system call 
    // is a pointer to an array of the 6 arguments
    if (num == SYS_mmap)
    {
        ADDRINT * mmapArgs = reinterpret_cast<ADDRINT *>(arg0);
        arg0 = mmapArgs[0];
        arg1 = mmapArgs[1];
        arg2 = mmapArgs[2];
        arg3 = mmapArgs[3];
        arg4 = mmapArgs[4];
        arg5 = mmapArgs[5];
    }
#endif
	ThreadData* tdata = get_tls(threadid);
	//avoid racing over ostream and CheckIfUnique()
	PIN_LockClient();
	ostringstream ostream;
	ostream << ip << " " << num << "(" << arg0 << " " << arg1 << " " << arg2 << " " 
			<< arg3 << " " << arg4 << " " << arg5 << ")\n";
	string str = ostream.str();	
	CheckIfUnique(str);
	PIN_UnlockClient();
	
	fprintf(tdata->Trace_File,"%s", str.c_str());
	fflush(tdata->Trace_File);
//    fprintf(tdata->Trace_File,"0x%lx, %ld(0x%lx, 0x%lx, 0x%lx, 0x%lx, 0x%lx, 0x%lx)\n",
//        (unsigned long)ip, (long)num,(unsigned long)arg0,(unsigned long)arg1,
//		  (unsigned long)arg2, (unsigned long)arg3, (unsigned long)arg4, (unsigned long)arg5);
}


VOID Trace(TRACE trace, VOID *v)
{
	IMG img = IMG_FindByAddress(TRACE_Address(trace));
	if(!IMG_Valid(img)) return;
	for (BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl))
	{
		for (INS ins = BBL_InsHead(bbl); INS_Valid(ins); ins = INS_Next(ins))
		{
			if (INS_IsSyscall(ins))
			{
				// Arguments and syscall number is only available before
				INS_InsertCall(ins, IPOINT_BEFORE, AFUNPTR(SysBefore),
					           IARG_INST_PTR, //IARG_THREAD_ID,
					           IARG_SYSCALL_NUMBER,
					           IARG_SYSARG_VALUE, 0, IARG_SYSARG_VALUE, 1,
					           IARG_SYSARG_VALUE, 2, IARG_SYSARG_VALUE, 3,
					           IARG_SYSARG_VALUE, 4, IARG_SYSARG_VALUE, 5,
					           IARG_THREAD_ID, IARG_END);

			}
		}
    }
}

/* ===================================================================== */
VOID ThreadStart(THREADID threadid, CONTEXT *ctxt, INT32 flags, VOID *v)
{
	//Create a Trace_File for each thread to avoid racing
	PIN_GetLock(&Lock, threadid+1);
	Num_Threads++;
	PIN_ReleaseLock(&Lock);
	ThreadData* tdata = new ThreadData;
	PIN_SetThreadData(Tls_Key, tdata, threadid);
	ostringstream os;
	string file_name = File_Name;
	os << (threadid+1);
	file_name += os.str();//thread id
	file_name += ".out";
	tdata->Trace_File = fopen(file_name.c_str(),"w");	
} 

/* ===================================================================== */

VOID Fini(INT32 code, VOID *v)
{
	//Close Trace_File of each thread
	ThreadData* tdata;
	for(int t = 0; t < Num_Threads; t++)
	{
	 	tdata = get_tls(t);
	 	fclose(tdata->Trace_File);
	}
	//print timestamps of unique traces
	FILE* tm_unq_file = fopen("strace_tm.txt", "w");
	for ( auto it = Time_Unq_Set.begin(); it != Time_Unq_Set.end(); ++it )
	{
		fprintf(tm_unq_file, "%.3f\n", *it);
	}
	fclose(tm_unq_file);
}                                                                 
/* ===================================================================== */

int main(int argc, char *argv[])
{
    PIN_Init(argc, argv);
	PIN_InitLock(&Lock);
    TRACE_AddInstrumentFunction(Trace, 0);
    PIN_AddThreadStartFunction(ThreadStart, 0);
    PIN_AddFiniFunction(Fini, 0);
    // Never returns
    PIN_StartProgram();
    return 0;
}
