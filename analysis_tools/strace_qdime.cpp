/*
 *  This file contains an ISA-portable PIN tool for tracing system calls
 */

#include <stdio.h>

#if defined(TARGET_MAC)
#include <sys/syscall.h>
#elif !defined(TARGET_WINDOWS)
#include <syscall.h>
#endif

#include "pin.H"
#include <time.h>
#include <unordered_set>
#include <set>
#include "qdime.h"

string File_Name = "strace_qdime";//output file name

/* ===================================================================== */
#define sec_to_nsec 1000000000//from second to nanosecond
struct timespec tm;//print timestamp to measure info gain with time
//save unique traces in unordered set; it's faster than set
std::unordered_set<string> Trace_Set;
std::unordered_set<string>::const_iterator Iter_Trace;
//save timestamps in ordered set; I care about the ascending order
//timestamps of unique traces
std::set< double> Time_Unq_Set;
std::set< double>::const_iterator Iter_Unq_Time;
//Check if the extracted trace is unique (not redundant)
//long double test[10000000];
//int indx = 0;
static inline void CheckIfUnique(string str)
{
	clock_gettime( CLOCK_MONOTONIC, &tm);
	if ( Trace_Set.find(str) == Trace_Set.end())
	{	
		//if not found i.e., new unique trace
		//add the trace to the unordered_set 
		Trace_Set.insert(str);
		//and add the timestamp to the set
		Time_Unq_Set.insert(tm.tv_sec + ( double)tm.tv_nsec/sec_to_nsec);
	}
}

/* ===================================================================== */


// Print syscall number and arguments
VOID SysBefore(ADDRINT ip, ADDRINT num, ADDRINT arg0, ADDRINT arg1, ADDRINT arg2, ADDRINT arg3, ADDRINT arg4, ADDRINT arg5, THREADID threadid)
{
#if defined(TARGET_LINUX) && defined(TARGET_IA32) 
    // On ia32 Linux, there are only 5 registers for passing system call arguments, 
    // but mmap needs 6. For mmap on ia32, the first argument to the system call 
    // is a pointer to an array of the 6 arguments
    if (num == SYS_mmap)
    {
        ADDRINT * mmapArgs = reinterpret_cast<ADDRINT *>(arg0);
        arg0 = mmapArgs[0];
        arg1 = mmapArgs[1];
        arg2 = mmapArgs[2];
        arg3 = mmapArgs[3];
        arg4 = mmapArgs[4];
        arg5 = mmapArgs[5];
    }
#endif
	ThreadData* tdata = get_tls(threadid);
	//avoid racing over ostream and CheckIfUnique()
	PIN_LockClient();
	ostringstream ostream;
	ostream << ip << " " << num << "(" << arg0 << " " << arg1 << " " << arg2 << " " 
			<< arg3 << " " << arg4 << " " << arg5 << ")\n";
	string str = ostream.str();	
	CheckIfUnique(str);
	PIN_UnlockClient();
	
	fprintf(tdata->Trace_File,"%s", str.c_str());
	fflush(tdata->Trace_File);
//    fprintf(tdata->Trace_File,"0x%lx, %ld(0x%lx, 0x%lx, 0x%lx, 0x%lx, 0x%lx, 0x%lx)\n",
//        (unsigned long)ip, (long)num,(unsigned long)arg0,(unsigned long)arg1,
//		  (unsigned long)arg2, (unsigned long)arg3, (unsigned long)arg4, (unsigned long)arg5);
}


VOID Trace(TRACE trace, VOID *v)
{
	UINT64 trace_addr = TRACE_Address(trace);
	IMG img = IMG_FindByAddress(trace_addr);
	if(!IMG_Valid(img)) return;
	ADDRINT version = TRACE_Version(trace);

	UINT64 img_low_addr = IMG_LowAddress(img);	
	UINT64 trace_rel_addr = trace_addr - img_low_addr;
	USIZE trace_size = TRACE_Size(trace);
	THREADID thread_id = PIN_ThreadId();
	bool Allow_Instrum = 1;
	if(Redun_Suppress)
	{
		Allow_Instrum = dime_compare_to_log(thread_id, trace_rel_addr, trace_size);
	}
	
	if(Allow_Instrum)
	{
		for (BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl))
		{
			for (INS ins = BBL_InsHead(bbl); INS_Valid(ins); ins = INS_Next(ins))
			{
				if (INS_IsSyscall(ins))
				{
					dime_switch_version(version, ins);
					switch(version) {
					  case VERSION_BASE:
					  	//Do Nothing 
						break;
					  case VERSION_INSTRUMENT:
						// Arguments and syscall number is only available before
						INS_InsertCall(ins, IPOINT_BEFORE, AFUNPTR(SysBefore),
								IARG_INST_PTR, //IARG_THREAD_ID,
								IARG_SYSCALL_NUMBER,
								IARG_SYSARG_VALUE, 0, IARG_SYSARG_VALUE, 1,
								IARG_SYSARG_VALUE, 2, IARG_SYSARG_VALUE, 3,
								IARG_SYSARG_VALUE, 4, IARG_SYSARG_VALUE, 5,
								IARG_THREAD_ID, IARG_END);
						break;
					  default:
						assert(0); 
						break;
					}
				}
			}
		}
		dime_modify_log(version, thread_id, trace_rel_addr, trace_size);
	}
}

/* ===================================================================== */

VOID ThreadStart(THREADID threadid, CONTEXT *ctxt, INT32 flags, VOID *v)
{
	dime_thread_start(threadid);
	ThreadData* tdata = get_tls(threadid);
	ostringstream os;
	string file_name = File_Name;
	os << (threadid+1);
	file_name += os.str();//thread id
	file_name += ".out";
	tdata->Trace_File = fopen(file_name.c_str(),"w");
}

/* ===================================================================== */

VOID Fini(INT32 code, VOID *v)
{
//	for(int i = 0; i < indx; i++)
//	{
//		Trace_File2 << test[i] << endl;
//	}
	dime_fini();
	//Close Trace_File of each thread
	ThreadData* tdata;
	for(int t = 0; t < Num_Threads; t++)
	{
	 	tdata = get_tls(t);
	 	fclose(tdata->Trace_File);
	}
	//print timestamps of unique traces
	FILE* tm_unq_file = fopen("strace_qdime_tm.txt", "w");
	for ( auto it = Time_Unq_Set.begin(); it != Time_Unq_Set.end(); ++it )
	{
		fprintf(tm_unq_file, "%.3f\n", *it);
	}
	fclose(tm_unq_file);
}
/* ===================================================================== */

int main(int argc, char *argv[])
{
    PIN_Init(argc, argv);
	dime_init((char*)"strace_qdime_extra.out");
TRACE_AddInstrumentFunction(Trace, 0);
    PIN_AddThreadStartFunction(ThreadStart, 0);
    PIN_AddFiniFunction(Fini, 0);
    PIN_StartProgram();
    return 0;
}
